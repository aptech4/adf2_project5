import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        question3();
    }

    static void question3() {
        FirstThread firstThread = new FirstThread();
        SecondThread secondThread = new SecondThread();

        firstThread.start();
        secondThread.start();
    }

    static void question2() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("1. Thêmmớisinhviên.\n" +
                    "2. Danhsáchsinhviên.\n" +
                    "3. Tìmkiếmsinhviêntheotên.\n" +
                    "4. Thoát.\n" +
                    "Nhập tuỳ chọn: ");
            int option = 0;
            try {
                option = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Tuỳ chọn bạn nhập không hợp lệ!");
                continue;
            }

            switch (option) {
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("Nhập tên sinh viên: ");
                    String name = scanner.nextLine();
                    System.out.print("Nhập mã sinh viên: ");
                    String code = scanner.nextLine();
                    System.out.print("Nhập số điện thoại: ");
                    String phone = scanner.nextLine();

                    Student student = new Student(name, code, phone);
                    saveStudentToFile(student);
                    break;
                case 2:
                    ArrayList<Student> students = readStudentFromFile();
                    for(Student st : students) {
                        System.out.println(st.toString());
                    }
                    break;
                case 3:
                    scanner = new Scanner(System.in);
                    System.out.print("Nhập tên sinh viên cần tìm: ");
                    String searchName = scanner.nextLine();

                    ArrayList<Student> studentList = readStudentFromFile();//Danh sach sinh vien trong database
                    ArrayList<Student> results = new ArrayList<>();//Ket qua tim kiem

                    for(Student st: studentList) {
                        if(st.getStudentName().toLowerCase().contains(searchName.toLowerCase())) {
                            results.add(st);
                        }
                    }

                    System.out.println("Kết quả tìm kiếm:");
                    for(Student st : results) {
                        System.out.println(st.toString());
                    }

                    break;
                case 4:
                    return;
                default:
                    break;
            }
        }
    }

    static void saveStudentToFile(Student student) {
        File file = new File("student.dat");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Không thể khởi tạo file.");
            }
        }

        try {
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write(student.toString() + "\n");
            fileWriter.close();
            System.out.println("Đã lưu thành công!");
        } catch (IOException e) {
            System.out.println("Không thể ghi file.");
        }
    }

    static ArrayList<Student> readStudentFromFile() {
        ArrayList<Student> students = new ArrayList<>();

        File file = new File("student.dat");
        if (!file.exists()) {
            return students;
        }

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] elements = line.split("-");
                if(elements.length != 3) {
                    continue;
                }
                Student st = new Student(elements[0].trim(), elements[1].trim(), elements[2].trim());
                students.add(st);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        return students;
    }
}