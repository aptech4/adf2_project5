public class Student {
    private String studentName;
    private String studentCode;
    private String studentPhone;

    public Student() {
    }

    public Student(String studentName, String studentCode, String studentPhone) {
        this.studentName = studentName;
        this.studentCode = studentCode;
        this.studentPhone = studentPhone;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    @Override
    public String toString() {
        return this.studentName + " - " + this.studentCode + " - " + this.studentPhone;
    }
}
